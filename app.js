const express = require('express');
const app = express();

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

//Configurar los Middleware
//Este método se llama como middleware
app.use(express.json());
//es un método incorporado en express para reconocer el objeto de solicitud entrante como objeto JSON.
//Este método se llama como middleware
app.use(express.urlencoded({ extended: true }));

//CONFIGURAR SWAGGER
const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: '1er Sprint Acmamica',
        version: '1.0.0'
      }
    },
    apis: ['./app.js'],
  };

//CONFIGURAR EL SwaggerDocs
  const swaggerDocs = swaggerJsDoc(swaggerOptions);

//CONFIGURAR SERVER PARA EL USO DE SWAGGER, O CONFIGURAR EL ENDPOINT

app.use('/api-docs',
   swaggerUI.serve,
   swaggerUI.setup(swaggerDocs));

//ARRAY USUARIOS
let users = [{
    user: "admin",
    pass: "admin",
    nombreApellido: "",
    telefono: "",
    direccion: "",
    email: "",
    admin: true,
    logueado: false
}];

let userLogueado = "";
let userId = "";

// const new_user = {
//     user: "",
//     pass: "",
//     nombreApellido: "",
//     telefono: "",
//     direccion: "",
//     email: "",
//     admin: false
// };

//ARRAY PRODUCTOS
let products = [{
    productID : 1,
    nombre: "Bagel de Salmón",
    precio: "425"
},{
    productID : 2,
    nombre: "Hamburguesa Clásica",
    precio: "350"
},{
    productID : 3,
    nombre: "Sandwich Veggie",
    precio: "310"
},{
    productID : 4,
    nombre: "Ensalada Veggie",
    precio: "340"
},{
    productID : 5,
    nombre: "Focaccia",
    precio: "300"
},{
    productID : 6,
    nombre: "Sandwich Focaccia",
    precio: "440"
}];

const new_product = {
    id: "",
    nombre: "",
    precio: "",
};

//ARRAY PEDIDOS
let order = [{
    userID: "",
    orderID: "",
    items: [],
    pedidoNuevo: true,
    enPreparacion: false,
    enviado: false,
    total: ""
}];

const new_order = {
    userID: "",
    orderID: "",
    items: [],
    pedidoNuevo: true,
    enPreparacion: false,
    enviado: false,
    total: ""
};

//MIDDLEWARES

const validarUsuario = (req,res,next) => {
    let valUser = req.body.user;
    console.log(valUser)
    users.forEach(usuario => {
        if(valUser == usuario.user){
            next();
        };
    });
    return res.send('El usuario ingresado no existe');
};

const validarPass = (req,res,next) => {
    let valPass = req.body.pass;
    users.forEach(password => {
        if(valPass == password.pass){
            next();
        };
    });
    return res.send('La contraseña es incorrecta');
};

const emailDuplicado = (req,res,next) => {
    let emailDup = req.body.email;
    users.forEach(usuario => {
        if(emailDup == usuario.email){
            return res.send('El email ya existe');
        }
    });
    next();
};

const esAdmin = (req,res,next) => {
    users.forEach(usuario => {
        if(usuario.admin == true){
            next();
        }
    });
    return res.send('El usuario no es administrador');
};

const isLogueado = (req,res,next) => {
    users.forEach(usuario => {
        if(usuario.logueado === true){
            next();
        }
    });
    return res.send('Usuario no logueado');
};

//ENDPOINTS

//LOGIN

/**
 * @swagger
 * /login:
 *  post:
 *    description: Login de usuario
 *    parameters:
 *    - name: user
 *      description: Usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: pass
 *      description: Contraseña
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */
app.post('/login',validarUsuario,validarPass,(req,res)=>{
    let userLogin = req.body.user;
    let passLogin = req.body.pass;
    users.forEach(user => {
        if(userLogin == user.user && passLogin == user.pass){
                userID = users.indexOf(user);
                user.logueado = true;
                userLogueado = user.user;
                return res.send('Usuario logueado correctamente')
        };
    });
    //res.send('Hubo un error en el login.');
});

/**
 * @swagger
 * /login/new:
 *  post:
 *    description: Creación de usuario
 *    parameters:
 *    - name: user
 *      description: Usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: pass
 *      description: Contraseña
 *      in: formData
 *      required: true
 *      type: string
 *    - name: nombreApellido
 *      description: Nombre y Apellido
 *      in: formData
 *      required: true
 *      type: string
 *    - name: telefono
 *      description: Telefono
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: direccion
 *      description: Dirección
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.post('/login/new',emailDuplicado,(req,res)=>{
    const {user, pass, nombreApellido, telefono, direccion, email} = req.body;
    const new_user = {
        user: user,
        pass: pass,
        nombreApellido: nombreApellido,
        telefono: telefono,
        direccion: direccion,
        email: email,
        admin: false,
        logueado: false
    };
    users.push(new_user);
    console.log('Usuario creado exitosamente');
    return res.send(new_user);
});

/**
 * @swagger
 * /users:
 *  get:
 *    description: Enlista usuarios
 *    responses:
 *      200:
 *        description: Success
 * 
 */

// GET USUARIOS
app.get('/users',(req,res)=>{
    res.send(users)
})

// PRODUCTOS
/**
 * @swagger
 * /productos:
 *  get:
 *    description: Enlista productos
 *    responses:
 *      200:
 *        description: Success
 * 
 */
// GET PRODUCTOS USUARIO
app.get('/productos',isLogueado,(req,res)=>{
    res.send(products)
})

/**
 * @swagger
 * /productos/new:
 *  post:
 *    description: Creación de producto
 *    parameters:
 *    - name: nombre
 *      description: Nombre del producto
 *      in: formData
 *      required: true
 *      type: string
 *    - name: precio
 *      description: Precio del producto
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

app.post('/productos/new/',esAdmin,(req,res)=>{
    const {nombre, precio} = req.body;
    const new_product = {
        id: (products.length)+1,
        nombre: nombre,
        precio: precio
    };
    
    users.push(new_product);
    console.log('Producto creado exitosamente');
    return res.send(new_product);
    });


app.listen(3000, () => console.log('Corriendo en el puerto 3000'));
